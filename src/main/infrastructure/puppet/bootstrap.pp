tomcat::install { '/opt/tomcat':
  source_url => 'https://www.apache.org/dist/tomcat/tomcat-9/v9.0.5/bin/apache-tomcat-9.0.5.tar.gz',
}
tomcat::instance { 'default':
  catalina_home => '/opt/tomcat',
}

tomcat::war { $deploymentfilename:
  catalina_base => '/opt/tomcat',
  war_source    => "/$deploymentfilename",
}
